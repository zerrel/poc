package com.eloy.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class AuthorService {

    private final RestTemplate restTemplate;
    private final AuthorMapper authorMapper;

    @Value("${base.url}")
    private String baseUrl;
    private HttpEntity<Author> entity;

    @Autowired
    public AuthorService(RestTemplate restTemplate, AuthorMapper authorMapper) {
        this.restTemplate = restTemplate;
        this.authorMapper = authorMapper;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<>(headers);
    }

    public List<AuthorResponse> findAll() {
        ResponseEntity<List<Author>> exchange = restTemplate.exchange(
                null + "/author/search/all",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Author>>() {
                },
                new Object[]{}
        );

        return authorMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/author/search/all",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Author>>() {
                },
                new Object[]{}
        ).getBody());
    }

    public AuthorResponse findById(Long id) {
        return authorMapper.toResponse(restTemplate.exchange(
                baseUrl + "/author/search/id=" + id,
                HttpMethod.GET, entity,
                Author.class
        ).getBody());
    }

    public List<AuthorResponse> findByFirstName(String firstName) {
        return authorMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/author/search/first-name=" + firstName,
                HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<Author>>() {}
        ).getBody());
    }

    public List<AuthorResponse> findByLastName(String lastName) {
        return authorMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/author/search/last-name=" + lastName,
                HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<Author>>() {}
        ).getBody());
    }

    public List<AuthorResponse> findByInitials(String initials) {
        return authorMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/author/search/initials=" + initials,
                HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<Author>>() {}
        ).getBody());
    }

    public AuthorResponse create(Author author) {
        return authorMapper.toResponse(restTemplate.postForObject(baseUrl + "/author/new",
                author,
                Author.class
        ));
    }

    public AuthorResponse update(Long id, AuthorResponse author) {
        try {
            restTemplate.put(baseUrl + "/edit/id=" + id, author);
            return author;
        } catch (Exception e) {
            return new AuthorResponse();
        }
    }

    public String delete(Long id) {
        //TODO
        return null;
    }
}
