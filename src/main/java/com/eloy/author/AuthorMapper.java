package com.eloy.author;

import com.eloy.literature.LiteratureResponse;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public abstract class AuthorMapper {

    public abstract Author toEntity(AuthorResponse response);

    Long toEntity(LiteratureResponse response) {
        return response.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<Author> toEntityList(List<AuthorResponse> responseList);

    @Mapping(target = "literature", ignore = true)
    public abstract AuthorResponse toResponse(Author author);

    @Mapping(target = "id", source = "authorId")
    public abstract AuthorResponse toResponse(Author author, Long authorId);

    LiteratureResponse toResponse(Long literatureId) {
        return new LiteratureResponse().id(literatureId);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<AuthorResponse> toResponseList(List<Author> authorList);
}
