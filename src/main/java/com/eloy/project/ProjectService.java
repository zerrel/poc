package com.eloy.project;

import com.eloy.util.ProjectState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ProjectService {

    private static final Logger logger = LoggerFactory.getLogger(ProjectService.class);
    private final UserProjectService userProjectService;
    private final RestTemplate restTemplate;
    private final ProjectMapper projectMapper;
    private HttpEntity<Project> entity;

    @Value("${base.url}")
    private String baseUrl;

    @Autowired
    public ProjectService(RestTemplate restTemplate, ProjectMapper projectMapper, UserProjectService userProjectService) {
        this.restTemplate = restTemplate;
        this.projectMapper = projectMapper;
        this.userProjectService = userProjectService;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<>(headers);
    }

    public List<ProjectResponse> findAll() {
        return projectMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/project/search/all",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Project>>() {
                }
        ).getBody());
    }

    public ProjectResponse findById(Long id) {
        Project project = restTemplate.exchange(
                baseUrl + "/project/search/id=" + id,
                HttpMethod.GET,
                entity,
                Project.class
        ).getBody();

        return userProjectService.getProjectWithUsers(project);
    }

    public ProjectResponse findByName(String name) {
        Project project = restTemplate.exchange(
                baseUrl + "/project/search/name=" + name,
                HttpMethod.GET,
                entity,
                Project.class
        ).getBody();

        return userProjectService.getProjectWithUsers(project);
    }

    public List<ProjectResponse> findByState(ProjectState state) {
        return projectMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/project/search/state=" + state,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Project>>() {
                }
        ).getBody());
    }

    public ProjectResponse create(Project project) {
        return projectMapper.toResponse(restTemplate.postForObject(
                baseUrl + "/project/new",
                project,
                Project.class));
    }

    public ProjectResponse update(Long id, Project project) {
        ProjectResponse response = null;
        try {
            restTemplate.put(baseUrl + "/project/edit/id=" + id, project);
            response = userProjectService.getProjectWithUsers(project);
        } catch (HttpClientErrorException e) {
            logger.debug(e.getLocalizedMessage());
        }
        return response;
    }

    public String delete(Long id) {
        try {
            restTemplate.delete(baseUrl + "/project/delete/id=" + id);
            return "ProjectResponse with id " + id + " has been deleted";
        } catch (HttpClientErrorException e) {

            return "Deletion error:\n" + e.getMessage();
        }
    }
}