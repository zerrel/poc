package com.eloy.project;

import com.eloy.user.User;
import com.eloy.user.UserResponse;
import com.eloy.user.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserProjectService {

    private final RestTemplate restTemplate;
    private final ProjectMapper projectMapper;
    private final UserMapper userMapper;
    @Value("${base.url}")
    private String baseUrl;
    private Logger logger = LoggerFactory.getLogger(UserProjectService.class);


    @Autowired
    public UserProjectService(RestTemplate restTemplate, ProjectMapper projectMapper, UserMapper userMapper) {
        this.restTemplate = restTemplate;
        this.projectMapper = projectMapper;
        this.userMapper = userMapper;

    }

    ProjectResponse getProjectWithUsers(Project project) {
        ProjectResponse response = projectMapper.toResponse(project);
        List<UserResponse> members = null;
        try {
            members = project
                    .getMembers()
                    .stream()
                    .map(this::fetchUser)
                    .map(userMapper::toResponse)
                    .collect(Collectors.toList());
            // TODO
        } catch (Exception e) {
            logger.debug(e.getLocalizedMessage());
        }
        response.setMembers(members);
        return response;
    }

    private User fetchUser(Long m) {
        User user = null;
        try {
            user = restTemplate.getForObject(baseUrl + "/user/search/id={id}", User.class, m);
        } catch (Exception e) {
            logger.debug(e.getLocalizedMessage());
        }
        return user;
    }
}
