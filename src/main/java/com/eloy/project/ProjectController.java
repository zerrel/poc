package com.eloy.project;

import com.eloy.util.ProjectState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/search/all")
    public ResponseEntity<List<ProjectResponse>> findAllProjects() {
        return new ResponseEntity<>(projectService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/search/id={id}")
    public ResponseEntity<ProjectResponse> findById(@PathVariable(value = "id") java.lang.Long id) {
        return new ResponseEntity<>(projectService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/search/name={name}")
    public ResponseEntity<ProjectResponse> findByName(@PathVariable(value = "name") String name) {
        return new ResponseEntity<>(projectService.findByName(name), HttpStatus.OK);
    }

    @GetMapping("/search/state={state}")
    public ResponseEntity<List<ProjectResponse>> findByState(@PathVariable(value = "state") ProjectState state) {
        return new ResponseEntity<>(projectService.findByState(state), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<ProjectResponse> createProject(@RequestBody Project project) {
        return new ResponseEntity<>(projectService.create(project), HttpStatus.OK);
    }

    @PutMapping("/edit/id={id}")
    public ResponseEntity<?> updateProject(@PathVariable(value = "id") java.lang.Long id, @RequestBody Project projectDto) {
        return new ResponseEntity<>(projectService.update(id, projectDto), HttpStatus.OK);
    }

    @RequestMapping("/delete/id={id}")
    public ResponseEntity<String> deleteProject(@PathVariable(value = "id") java.lang.Long id) {
        return new ResponseEntity<>(projectService.delete(id), HttpStatus.OK);
    }
}
