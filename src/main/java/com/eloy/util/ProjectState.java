package com.eloy.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets ProjectState
 */
public enum ProjectState {
    IN_PREPARATION("In Preparation"),
    IN_PROGRESS("In Progress"),
    ON_HOLD("On Hold"),
    COMPLETED("Completed"),
    OTHER("Other");

    private String value;

    ProjectState(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ProjectState fromValue(String text) {
        for (ProjectState b : ProjectState.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }
}
