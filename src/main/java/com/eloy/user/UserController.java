package com.eloy.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/search/all")
    public ResponseEntity<List<UserResponse>> findAllUsers() {
        return new ResponseEntity<>((userService.findAll()), HttpStatus.OK);
    }

    @GetMapping("/search/id={id}")
    public ResponseEntity<UserResponse> findById(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(userService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/search/username={username}")
    public ResponseEntity<UserResponse> findByUsername(@PathVariable(value = "username") String username) {
        return new ResponseEntity<>(userService.findByUsername(username), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<UserResponse> create(@RequestBody User user) {
        return new ResponseEntity<>(userService.create(user), HttpStatus.OK);
    }

    @PutMapping("/edit/id={id}")
    public ResponseEntity<UserResponse> update(@PathVariable(value = "id") Long id, @RequestBody UserResponse user) {
        return new ResponseEntity<>(userService.update(id, user), HttpStatus.OK);
    }

    @RequestMapping("/delete/id={id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(userService.delete(id), HttpStatus.OK);
    }
}