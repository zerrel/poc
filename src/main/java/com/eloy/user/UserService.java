package com.eloy.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class UserService {

    private final RestTemplate restTemplate;
    private final UserMapper userMapper;
    @Value("${base.url}")
    private String baseUrl;
    private HttpEntity<UserResponse> entity;

    @Autowired
    public UserService(RestTemplate restTemplate, UserMapper userMapper) {
        this.restTemplate = restTemplate;
        this.userMapper = userMapper;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<>(headers);
    }

    public List<UserResponse> findAll() {
        return userMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/user/search/all",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<User>>() {
                }
        ).getBody());
    }

    public UserResponse findById(Long id) {
        return userMapper.toResponse(restTemplate.exchange(
                baseUrl + "/user/search/id=" + id,
                HttpMethod.GET,
                entity,
                User.class
        ).getBody());
    }

    public UserResponse findByUsername(String username) {
        return userMapper.toResponse(restTemplate.exchange(
                baseUrl + "/user/search/username=" + username,
                HttpMethod.GET,
                entity,
                User.class
        ).getBody());
    }

    public UserResponse create(User user) {
        return userMapper.toResponse(restTemplate.postForObject(
                baseUrl + "/user/new"
                , user
                , User.class));
    }

    public UserResponse update(Long id, UserResponse userDto) {
        try {
            restTemplate.put(baseUrl + "/user/edit/id=" + id, userDto);
            return userDto;
        } catch (HttpClientErrorException e) {
            return null;
        }
    }

    public String delete(Long id) {
        try {
            restTemplate.delete(baseUrl + "/user/delete/id=" + id);
            return "UserResponse with id " + id + " has been deleted";
        } catch (HttpClientErrorException e) {

            return "Deletion error:\n" + e.getMessage();
        }
    }
}
