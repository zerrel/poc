package com.eloy.user;

import com.eloy.project.ProjectResponse;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class UserMapper {

    @Mapping(target = "projects", ignore = true)
    public abstract User toEntity(UserResponse response);

    Long toEntity(ProjectResponse project) {
        return project.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<User> toEntityList(List<UserResponse> responseList);

    @Mapping(target = "projects", ignore = true)
    public abstract UserResponse toResponse(User user);

    @Mapping(target = "id", source = "userId")
    public abstract UserResponse toResponse(User user, Long userId);

    ProjectResponse toResponse(Long projectId) {
        return new ProjectResponse().id(projectId);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<UserResponse> toResponseList(List<User> responseList);
}
