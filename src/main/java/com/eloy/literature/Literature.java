package com.eloy.literature;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Literature
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-02-28T16:14:19.667+01:00[Europe/Paris]")
public class Literature {
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("authors")
    @Valid
    private List<Long> authors = null;

    @JsonProperty("poi")
    private String poi = null;

    @JsonProperty("isbn")
    private String isbn = null;

    @JsonProperty("datePublished")
    private LocalDate datePublished = null;

    @JsonProperty("url")
    private String url = null;

    @JsonProperty("peerReviewed")
    private Integer peerReviewed = null;

    public Literature id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Literature title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     *
     * @return title
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Size(min = 1)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Literature authors(List<Long> authors) {
        this.authors = authors;
        return this;
    }

    public Literature addAuthorsItem(Long authorsItem) {
        if (this.authors == null) {
            this.authors = new ArrayList<Long>();
        }
        this.authors.add(authorsItem);
        return this;
    }

    /**
     * Get authors
     *
     * @return authors
     **/
    @ApiModelProperty(value = "")

    public List<Long> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Long> authors) {
        this.authors = authors;
    }

    public Literature poi(String poi) {
        this.poi = poi;
        return this;
    }

    /**
     * Get poi
     *
     * @return poi
     **/
    @ApiModelProperty(value = "")

    public String getPoi() {
        return poi;
    }

    public void setPoi(String poi) {
        this.poi = poi;
    }

    public Literature isbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    /**
     * Get isbn
     *
     * @return isbn
     **/
    @ApiModelProperty(value = "")

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Literature datePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
        return this;
    }

    /**
     * Get datePublished
     *
     * @return datePublished
     **/
    @ApiModelProperty(value = "")

    @Valid
    public LocalDate getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
    }

    public Literature url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    @ApiModelProperty(value = "")

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Literature peerReviewed(Integer peerReviewed) {
        this.peerReviewed = peerReviewed;
        return this;
    }

    /**
     * Get peerReviewed
     * minimum: -1
     * maximum: 1
     *
     * @return peerReviewed
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Min(-1)
    @Max(1)
    public Integer getPeerReviewed() {
        return peerReviewed;
    }

    public void setPeerReviewed(Integer peerReviewed) {
        this.peerReviewed = peerReviewed;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Literature literature = (Literature) o;
        return Objects.equals(this.id, literature.id) &&
                Objects.equals(this.title, literature.title) &&
                Objects.equals(this.authors, literature.authors) &&
                Objects.equals(this.poi, literature.poi) &&
                Objects.equals(this.isbn, literature.isbn) &&
                Objects.equals(this.datePublished, literature.datePublished) &&
                Objects.equals(this.url, literature.url) &&
                Objects.equals(this.peerReviewed, literature.peerReviewed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, authors, poi, isbn, datePublished, url, peerReviewed);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Literature {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    authors: ").append(toIndentedString(authors)).append("\n");
        sb.append("    poi: ").append(toIndentedString(poi)).append("\n");
        sb.append("    isbn: ").append(toIndentedString(isbn)).append("\n");
        sb.append("    datePublished: ").append(toIndentedString(datePublished)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("    peerReviewed: ").append(toIndentedString(peerReviewed)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
