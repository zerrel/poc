package com.eloy.literature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class LiteratureService {

    private final RestTemplate restTemplate;
    private final LiteratureMapper literatureMapper;
    private final AuthorLiteratureService authorLiteratureService;
    @Value("${base.url}")
    private String baseUrl;
    private Logger logger = LoggerFactory.getLogger(LiteratureService.class);
    private HttpEntity<Literature> entity;

    @Autowired
    public LiteratureService(RestTemplate restTemplate, LiteratureMapper literatureMapper, AuthorLiteratureService authorLiteratureService) {
        this.restTemplate = restTemplate;
        this.literatureMapper = literatureMapper;
        this.authorLiteratureService = authorLiteratureService;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<>(headers);
    }

    List<LiteratureResponse> findAll() {
        return literatureMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/literature/search/all",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Literature>>() {
                }
        ).getBody());
    }

    LiteratureResponse findById(Long id) {
        Literature literature = restTemplate.exchange(
                baseUrl + "/literature/search/id=" + id,
                HttpMethod.GET,
                null,
                Literature.class
        ).getBody();

        return authorLiteratureService.getLiteratureWithAuthors(literature);
//        return new LiteratureResponse().id(id).title("Hello, World!");
    }

    LiteratureResponse findByTitle(String title) {
        Literature literature = restTemplate.exchange(
                baseUrl + "/literature/search/title=" + title,
                HttpMethod.GET,
                null,
                Literature.class
        ).getBody();

        return authorLiteratureService.getLiteratureWithAuthors(literature);
    }

    List<LiteratureResponse> findByAuthor(Long author) {
        return literatureMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/literature/search/author=" + author,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Literature>>() {
                }
        ).getBody());
    }

    LiteratureResponse findByPoi(String poi) {
        Literature literature = restTemplate.exchange(
                baseUrl + "/literature/search/poi=" + poi,
                HttpMethod.GET,
                null,
                Literature.class
        ).getBody();

        return authorLiteratureService.getLiteratureWithAuthors(literature);
    }

    LiteratureResponse findByIsbn(String isbn) {
        Literature literature = restTemplate.exchange(
                baseUrl + "/literature/search/isbn=" + isbn,
                HttpMethod.GET,
                null,
                Literature.class
        ).getBody();

        return authorLiteratureService.getLiteratureWithAuthors(literature);
    }

    List<LiteratureResponse> findByDatePublished(LocalDate date) {
        return literatureMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/literature/search/date=" + date,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Literature>>() {
                }
        ).getBody());
    }

    List<LiteratureResponse> findByPeerReviewed(Integer peerReviewed) {
        return literatureMapper.toResponseList(restTemplate.exchange(
                baseUrl + "/literature/search/peer-reviewed=" + peerReviewed,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Literature>>() {
                }
        ).getBody());
    }

    LiteratureResponse findByUrl(String url) {
        Literature literature = restTemplate.exchange(
                baseUrl + "/literature/search/url=" + url,
                HttpMethod.GET,
                null,
                Literature.class
        ).getBody();

        return authorLiteratureService.getLiteratureWithAuthors(literature);
    }

    LiteratureResponse create(Literature literature) {
        return literatureMapper.toResponse(restTemplate.postForObject(
                baseUrl + "/literature/new",
                literature,
                Literature.class));
    }

    LiteratureResponse update(Long id, Literature literature) {
        LiteratureResponse response = null;
        try {
            restTemplate.put(
                    baseUrl + "/literature/edit/id=" + id,
                    literature,
                    Literature.class);
            response = authorLiteratureService.getLiteratureWithAuthors(literature).id(id);
        } catch (HttpClientErrorException e) {
            logger.debug(e.getLocalizedMessage());
        }
        return response;
    }

    String delete(Long id) {
        try {
            restTemplate.delete(baseUrl + "/literature/delete/id=" + id);
            return "LiteratureResponse with id " + id + " has been deleted";
        } catch (HttpClientErrorException e) {
            logger.debug(e.getLocalizedMessage());
            return "Deletion error:\n" + e.getMessage();
        }
    }
}
