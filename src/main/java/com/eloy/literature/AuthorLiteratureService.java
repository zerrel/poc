package com.eloy.literature;

import com.eloy.author.Author;
import com.eloy.author.AuthorResponse;
import com.eloy.author.AuthorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorLiteratureService {

    private final RestTemplate restTemplate;
    private final LiteratureMapper literatureMapper;
    private final AuthorMapper authorMapper;
    @Value("${base.url}")
    private String baseUrl;
    private Logger logger = LoggerFactory.getLogger(AuthorLiteratureService.class);

    @Autowired
    public AuthorLiteratureService(RestTemplate restTemplate, LiteratureMapper literatureMapper, AuthorMapper authorMapper) {
        this.restTemplate = restTemplate;
        this.literatureMapper = literatureMapper;
        this.authorMapper = authorMapper;
    }

    LiteratureResponse getLiteratureWithAuthors(Literature literature) {
        LiteratureResponse response = literatureMapper.toResponse(literature);
        List<AuthorResponse> authors = null;
        try {
            authors = literature
                    .getAuthors()
                    .stream()
                    .map(this::fetchAuthor)
                    .map(authorMapper::toResponse)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            logger.debug(e.getLocalizedMessage());
        }
        response.setAuthors(authors);
        return response;
    }

    private Author fetchAuthor(Long authorId) {
        Author author = null;
        try {
            author = restTemplate.getForObject(baseUrl + "/author/search/id={id}", Author.class, authorId);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return author;

    }


}
