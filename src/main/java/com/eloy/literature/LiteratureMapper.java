package com.eloy.literature;

import com.eloy.author.AuthorResponse;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class LiteratureMapper {

    public abstract Literature toEntity(Literature literature);

    Long toEntity(AuthorResponse author) {
        return author.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<Literature> toEntityList(List<LiteratureResponse> responseList);

    @Mapping(target = "authors", ignore = true)
    public abstract LiteratureResponse toResponse(Literature literature);

    @Mapping(target = "id", source = "literatureId")
    public abstract LiteratureResponse toResponse(Literature literature, Long literatureId);

    AuthorResponse toResponse(Long authorId) {
        return new AuthorResponse().id(authorId);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<LiteratureResponse> toResponseList(List<Literature> literatureList);

}
