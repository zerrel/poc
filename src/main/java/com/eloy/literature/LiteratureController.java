package com.eloy.literature;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/literature")
public class LiteratureController {

    private final LiteratureService literatureService;


    @Autowired
    public LiteratureController(LiteratureService literatureService) {
        this.literatureService = literatureService;
    }


    @GetMapping("/search/all")
    public ResponseEntity<List<LiteratureResponse>> findAllLiterature() {
        return new ResponseEntity<>(
                literatureService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/search/id={id}")
    public ResponseEntity<LiteratureResponse> findById(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(literatureService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/search/title={title}")
    public ResponseEntity<LiteratureResponse> findByTitle(@PathVariable(value = "title") String title) {
        return new ResponseEntity<>(literatureService.findByTitle(title), HttpStatus.OK);
    }

    @GetMapping("/search/author={author}")
    public ResponseEntity<List<LiteratureResponse>> findByAuthor(@PathVariable(value = "author") Long id) {
        return new ResponseEntity<>(literatureService.findByAuthor(id), HttpStatus.OK);
    }

    @GetMapping("/search/poi={poi}")
    public ResponseEntity<LiteratureResponse> findByPoi(@PathVariable(value = "poi") String poi) {
        return new ResponseEntity<>(literatureService.findByPoi(poi), HttpStatus.OK);
    }

    @GetMapping("/search/isbn={isbn}")
    public ResponseEntity<LiteratureResponse> findByIsbn(@PathVariable(value = "isbn") String isbn) {
        return new ResponseEntity<>(literatureService.findByIsbn(isbn), HttpStatus.OK);
    }

    @GetMapping("/search/date-published={date}")
    public ResponseEntity<List<LiteratureResponse>> findByDatePublished(@PathVariable(value = "date") LocalDate date) {
        return new ResponseEntity<>(literatureService.findByDatePublished(date), HttpStatus.OK);
    }

    @GetMapping("/search/url={url}")
    public ResponseEntity<LiteratureResponse> findByUrl(@PathVariable(value = "url") String url) {
        return new ResponseEntity<>(literatureService.findByUrl(url), HttpStatus.OK);
    }

    @GetMapping("/search/peer-reviewed={peer-reviewed}")
    public ResponseEntity<List<LiteratureResponse>> findByPeerReviewed(@PathVariable(value = "peer-reviewed") java.lang.Integer peerReviewed) {
        return new ResponseEntity<>(literatureService.findByPeerReviewed(peerReviewed), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<LiteratureResponse> createLiterature(@RequestBody Literature literature) {
        return new ResponseEntity<>(literatureService.create(literature), HttpStatus.OK);
    }

    @PutMapping("/edit/id={id}")
    public ResponseEntity<LiteratureResponse> updateLiterature(@PathVariable(value = "id") Long id, @RequestBody Literature literature) {
        return new ResponseEntity<>(literatureService.update(id, literature), HttpStatus.OK);

    }

    @RequestMapping("/delete/id={id}")
    public ResponseEntity<String> deleteLiterature(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(literatureService.delete(id), HttpStatus.OK);
    }
}
