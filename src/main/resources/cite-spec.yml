openapi: 3.0.0
info:
  title: PoC API
  description: Open API Specification for Proof of Concept Application
  version: 0.0.1

servers:
  - url: http://localhost:8080
  - url: http://localhost:8070

paths:

  ##    ##
  # User #
  ##    ##
  /user/search/all:
    get:
      tags:
        - User
      summary: Returns a list of users.
      responses:
        "200":
          description: Gets an array of all users
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/User"
                uniqueItems: true
  /user/search/id={id}:
    get:
      tags:
        - User
      summary: Gets a user by id
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int32
          required: true
      responses:
        "200":
          description: User by id
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
  /user/search/username={username}:
    get:
      tags:
        - User
      summary: Gets a user by username
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
      responses:
        "200":
          description: User by username
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
  /user/new:
    post:
      tags:
        - User
      summary: Add a user
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/User"
            example:
              username: tommywiseau83
              password: supersecurepassword33
      responses:
        "201":
          description: Created
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
  /user/edit/id={id}:
    put:
      tags:
        - User
      summary: Updates a user
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/User"
            example:
              username: spiderman1
              password: supersecurepassword33
  /users/delete/id={id}:
    delete:
      tags:
        - User
      summary: Delete a user
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true

  ##      ##
  # Author #
  ##      ##
  /author/search/all:
    get:
      tags:
        - Author
      summary: Get all authors
      responses:
        "200":
          description: Gets an array of all authors
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Author"
                uniqueItems: true
  /author/search/id={id}:
    get:
      tags:
        - Author
      summary: Gets an Author by id
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true
      responses:
        "200":
          description: Author by id
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /author/search/first-name={first_name}:
    get:
      tags:
        - Author
      summary: Gets an author by first name
      parameters:
        - in: path
          name: first_name
          schema:
            type: string
          required: true
      responses:
        "200":
          description: Author by first name
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /author/search/last-name={last_name}:
    get:
      tags:
        - Author
      summary: Gets an author by last name
      parameters:
        - in: path
          name: last_name
          schema:
            type: string
          required: true
      responses:
        "200":
          description: Author by last name
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /author/search/initials={initials}:
    get:
      tags:
        - Author
    summary: Gets an author by initials
    parameters:
      - in: path
        name: initials
        schema:
          type: string
        required: true
    responses:
      "200":
        description: Author by initials
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Author"
  /author/new:
    post:
      tags:
        - Author
      summary: Add an author
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Author"
            example:
              first-name: Albert
              last-name: Einstein
              initials: A.
      responses:
        "201":
          description: Created
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /author/edit/id={id}:
    put:
      tags:
        - Author
      summary: Update an author
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Author"
            example:
              first-name: Thomas
              last-name: Edison
              initials: T.
  /author/delete/id={id}:
    delete:
      tags:
        - Author
      summary: Delete an author
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true

  ##       ##
  # Project #
  ##       ##
  /project/search/all:
    get:
      tags:
        - Project
      summary: Get all projects
      responses:
        "200":
          description: Gets an array of all projects
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Project"
                uniqueItems: true
  /project/search/id={id}:
    get:
      tags:
        - Project
      summary: Get project by id
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Project"
  /project/search/name={name}:
    get:
      tags:
        - Project
      summary: Get project by name
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Project"
  /project/search/state={state}:
    get:
      tags:
        - Project
      summary: Get projects by state
      parameters:
        - in: path
          name: state
          schema:
            $ref: "#/components/schemas/ProjectState"
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Project"
  /project/new:
    post:
      tags:
        - Project
      summary: Add a project
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Project"
            example:
              name: Yearly Report
              state: In Progress
      responses:
        "201":
          description: Created
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Project"
  /project/edit/id={id}:
    put:
      tags:
        - Project
      summary: Update a project
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Project"
            example:
              name: Yearly Report
              state: In Progress
  /project/delete/id={id}:
    delete:
      tags:
        - Project
      summary: Delete a project
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true

  ##          ##
  # Literature #
  ##          ##
  /literature/search/all:
    get:
      tags:
        - Literature
      summary: Get all literature
      responses:
        "200":
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Literature"
                uniqueItems: true
  /literature/search/id={id}:
    get:
      tags:
        - Literature
      summary: Get literature by id
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /literature/search/title={title}:
    get:
      tags:
        - Literature
      summary: Get literature by title
      parameters:
        - in: path
          name: title
          schema:
            type: string
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /literature/search/author={author}:
    get:
      tags:
        - Literature
      summary: Get literature by author
      parameters:
        - in: path
          name: author
          schema:
            type: string
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /literature/search/poi={poi}:
    get:
      tags:
        - Literature
      summary: Get literature by poi
      parameters:
        - in: path
          name: poi
          schema:
            type: string
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /literature/search/isbn={isbn}:
    get:
      tags:
        - Literature
      summary: Get literature by isbn
      parameters:
        - in: path
          name: isbn
          schema:
            type: string
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /literature/search/date-published={date}:
    get:
      tags:
        - Literature
      summary: Get literature by date published
      parameters:
        - in: path
          name: date
          schema:
            type: string
            format: date
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /literature/search/url={url}:
    get:
      tags:
        - Literature
      summary: Get literature by url
      parameters:
        - in: path
          name: url
          schema:
            type: string
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Author"
  /literature/search/peer-reviewed={peer_reviewed}:
    get:
      tags:
        - Literature
      summary: Get literature by peer reviewed
      parameters:
        - in: path
          name: peer_reviewed
          schema:
            type: integer
            format: int32
          required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Literature"
  /literature/new:
    post:
      tags:
        - Literature
      summary: Add literature
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Literature"
            example:
              title: 12 rules for life
              isbn: "9780241351642"
              date-published: "2018-01-16"
              peer-reviewed: 0
      responses:
        "201":
          description: Created
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Project"
  /literature/edit/id={id}:
    put:
      tags:
        - Literature
      summary: Update literature
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Literature"
            example:
              title: 12 rules for life
              isbn: "9780241351642"
              date-published: "2018-01-20"
              peer-reviewed: 0
  /literature/delete/id={id}:
    delete:
      tags:
        - Literature
      summary: Delete literature
      parameters:
        - in: path
          name: id
          schema:
            type: integer
            format: int64
          required: true
          
##          ##
# Components #
##          ##
components:
  schemas:
    Author:
      type: object
      properties:
        id:
          type: integer
          format: int64
        firstName:
          type: string
          minLength: 1
        lastName:
          type: string
          minLength: 1
        initials:
          type: string
          minLength: 1
          pattern: '^([A-Z]\.)+$'
        literature:
          type: array
          items:
            $ref: "#/components/schemas/Literature"
      required:
        - id
        - firstName
        - lastName
        - initials
    Literature:
      type: object
      properties:
        id:
          type: integer
          format: int64
        title:
          type: string
          minLength: 1
        authors:
          type: array
          items:
            $ref: "#/components/schemas/Author"
        references:
          type: integer
          format: int32
          nullable: true
        poi:
          type: string
          nullable: true
        isbn:
          type: string
          nullable: true
        datePublished:
          type: string
          format: date
          nullable: true
        url:
          type: string
          nullable: true
        peerReviewed:
          type: integer
          minimum: -1
          maximum: 1
      required:
        - id
        - title
        - peerReviewed
    User:
      type: object
      properties:
        id:
          type: integer
          format: int64
        username:
          type: string
          minLength: 1
        projects:
          type: array
          items:
            type: integer
            format: int64
      required:
        - id
        - username
    Project:
      type: object
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
        state:
          $ref: '#/components/schemas/ProjectState'
        subProjects:
          type: array
          items:
            $ref: "#/components/schemas/Project"
        members:
          type: array
          items:
            $ref: "#/components/schemas/User"
      required:
        - id
        - name
        - state
    ProjectState:
      type: string
      enum:
        - In Preparation
        - In Progress
        - On Hold
        - Completed
        - Other