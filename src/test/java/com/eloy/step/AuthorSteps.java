package com.eloy.step;

import com.eloy.IntegrationTestRunner;
import com.eloy.author.AuthorResponse;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class AuthorSteps extends IntegrationTestRunner {

    private static final Logger log = LoggerFactory.getLogger(AuthorSteps.class);
    private Map<String, Object> context = new HashMap<>();
    HttpEntity<AuthorResponse> entity;

    @Autowired
    private RestTemplate restTemplate;

    @Before
    public void setup() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<>(headers);
    }

    @Given("the following author:")
    public void theFollowingAuthor(Map<String, String> author) {
        context.put("author1", new AuthorResponse()
                .firstName(author.get("first name"))
                .lastName(author.get("last name"))
                .initials(author.get("initials")));
    }


    @When("the user adds that author")
    public void theUserAddsThatAuthor() {
        restTemplate.postForEntity(getEndpoint("author") + "/new", context.get("author1"), Void.class);
    }

    @Then("the user should be able to retrieve that author by first name {string}")
    public void theUserShouldBeAbleToRetrieveThatAuthorByFirstName(String firstName) {
        List<AuthorResponse> authors = restTemplate.exchange(
                getEndpoint("author") + "/search/first-name=" + firstName,
                HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<AuthorResponse>>() {
                }
        ).getBody();
        AuthorResponse author1 = (AuthorResponse)context.get("author1");
        assert authors != null;
        assertEquals(authors.get(0).getFirstName(), author1.getFirstName());

    }

    @And("the user should be able to retrieve that author by last name {string}")
    public void theUserShouldBeAbleToRetrieveThatAuthorByLastName(String lastName) {
        List<AuthorResponse> authors = restTemplate.exchange(
                getEndpoint("author") + "/search/last-name=" + lastName,
                HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<AuthorResponse>>() {
                }
        ).getBody();
        AuthorResponse author1 = (AuthorResponse)context.get("author1");
        assert authors != null;
        assertEquals(authors.get(0).getLastName(), author1.getLastName());
    }

    @And("the user should be able to retrieve that author by initials {string}")
    public void theUserShouldBeAbleToRetrieveThatAuthorByInitials(String initials) {
        List<AuthorResponse> authors = restTemplate.exchange(
                getEndpoint("author") + "/search/initials=" + initials,
                HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<AuthorResponse>>() {
                }
        ).getBody();
        AuthorResponse author1 = (AuthorResponse)context.get("author1");
        assert authors != null;
        assertEquals(authors.get(0).getInitials(), author1.getInitials());
    }

    @Given("the following invalid author:")
    public void theFollowingInvalidAuthor(Map<String, String> author) {
        context.put("author2", new AuthorResponse()
                .firstName(author.get("first name"))
                .lastName(author.get("last name"))
                .initials(author.get("initials")));
    }


    @Then("the user should not be able to retrieve that author by last name {string}")
    public void theUserShouldNotBeAbleToRetrieveThatAuthorByLastName(String lastName) {
        List<AuthorResponse> authors = new ArrayList<>();
        try {
            authors = restTemplate.exchange(
                    getEndpoint("author") + "/search/last-name=" + lastName,
                    HttpMethod.GET, entity,
                    new ParameterizedTypeReference<List<AuthorResponse>>() {
                    }
            ).getBody();
        } catch (EntityNotFoundException e) {
            log.warn(e.getLocalizedMessage());
        }
        AuthorResponse author2 = (AuthorResponse)context.get("author2");
        assert authors != null;
        assertEquals(authors.get(1).getLastName(), author2.getLastName());
    }
}
