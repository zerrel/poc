package com.eloy;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTestRunner {

    private static final Logger log = LoggerFactory.getLogger(IntegrationTestRunner.class);

    private final String GATEWAY_URL = "http://localhost:8070";


    public String getEndpoint(String endpoint) {
        return GATEWAY_URL + "/" + endpoint;
    }
}
