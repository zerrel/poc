package com.eloy.author;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

    private static final String BASE_URL = null;//"http://localhost:8090";

    private HttpEntity<Author> entity;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private AuthorMapper authorMapper;

    @InjectMocks
    private AuthorService testSubject;

    @Before
    public void setup() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        entity = new HttpEntity<>(headers);
    }

    @Test
    public void findAll_success() {
        List<Author> authors = new ArrayList<>();

        for(int i = 0; i < 5; i++) {
            authors.add(new Author().id((long)i).firstName("a" + i).lastName("b" + i).initials("a. b."));
        }

        ResponseEntity<List<Author>> responseEntity = new ResponseEntity<>(authors, HttpStatus.OK);

        when(restTemplate.exchange(
                eq(null + "/author/search/all"),
                any(HttpMethod.class),
                isNull(),
                any(ParameterizedTypeReference.class),
                eq(new Object[]{})))
                .thenReturn(responseEntity);
//        testSubject.findAll();

        ResponseEntity<List<Author>> exchange = restTemplate.exchange(
                null + "/author/search/all",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Author>>() {
                },
                new Object[1]
        );

        verify(restTemplate).exchange(
                eq(null + "/author/search/all"),
                eq(HttpMethod.GET),
                isNull(),
                any(ParameterizedTypeReference.class)
        );
    }
}