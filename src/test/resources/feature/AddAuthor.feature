Feature: Author Functionality

  Scenario Outline: Adding a valid author and searching for it by first name, last name and initials
    Given the following author:
      | first name  | <first name>  |
      | last name   | <last name>   |
      | initials    | <initials>    |
    When the user adds that author
    Then the user should be able to retrieve that author by first name "<first name>"
    And the user should be able to retrieve that author by last name "<last name>"
    And the user should be able to retrieve that author by initials "<initials>"

  Examples:
    | first name  | last name | initials  |
    | Aaa         | Bbb       | A. B.     |
    | Bbb         | Ccc       | B. C.     |
    | Ccc         | Ddd       | C. D.     |

  Scenario: Adding an invalid author
    Given the following invalid author:
      | first name  |       |
      | last name   | Doe   |
      | initials    | . D.  |
    When the user adds that author
    Then the user should not be able to retrieve that author by last name "<last name>"